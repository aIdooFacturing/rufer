package models


import java.sql.Blob

import anorm._
import anorm.SqlParser._
import com.sun.org.apache.bcel.internal.generic.IFNULL
import play.api.Play.current
import play.api.db
import play.api.db.DB
import play.http.websocket.Message.Binary

import scala.reflect.io.File

/**
  * Created by Hanyong Jo on 15. 3. 7..
  */
case class MemberService (EMP_BARCODE:String, EMP_CD :String,  DPT:String, NM:String,IMAGE:String,NOT_USE:Int)

object MemberService {

  /**
    * Using Parser Combinator
    */

  val simple: RowParser[MemberService] = {


    get[String]("EMP_BARCODE") ~
      get[String]("EMP_CD") ~
      get[String]("DPT") ~
      get[String]("NM") ~
      get[String]("IMAGE")~
      get[Int]("NOT_USE")map {
      case emp_barcode ~ emp_cd ~ dpt ~ nm  ~image ~not_use => MemberService(emp_barcode, emp_cd, dpt, nm,image,not_use)
    }

  }

  def getList[MemberService] = {
    DB.withConnection { implicit connection =>
      SQL(
        """

           SELECT
 |   IFNULL(EMP_BARCODE,'0') AS EMP_BARCODE
 |   ,EMP_CD
 |	,DPT
 |   ,NM
 |   ,IFNULL(IMAGE,'none.PNG') AS IMAGE
 |   ,NOT_USE
 |    FROM EMP_MST WHERE NOT_USE = 0;
 |
        """.stripMargin).as(MemberService.simple *)
    }
  }
}