package controllers

import javax.inject._
import play.api._
import play.api.mvc._
import models.MemberService

/**
  * This controller creates an `Action` to handle HTTP requests to the
  * application's home page.
  */




@Singleton
class HomeController @Inject() extends Controller {

  /**
    * Create an Action to render an HTML page with a welcome message.
    * The configuration in the `routes` file means that this method
    * will be called when the application receives a `GET` request with
    * a path of `/`.
    */
  def cardprint = Action {
    Ok(views.html.index("Your new application is ready."))
  }
  def list = Action {
    Ok(views.html.list(MemberService.getList))
  }
  def uploadFile = Action(parse.multipartFormData) { request =>
    request.body.file("image").map { picture =>
      import java.io.File
      val filename = picture.filename
      picture.ref.moveTo(new File(s"/tmp/picture/$filename"))
      Ok("파일 업로드 완료")
    }.getOrElse {
      Redirect(routes.HomeController.list).flashing(
        "error" -> "Missing file"
      )
    }
  }
}
